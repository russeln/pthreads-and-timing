
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h> 
#include <iostream>
#include <unistd.h>

#include "CStopWatch.h"

/*-------------------------------------------------------------------*/
void *getSamples(void* nSamples) {
   
   long numSamples = (long)nSamples;

   for(int i=0; i< numSamples; i++){
      usleep(std::rand() % 5000 + 1);
   }
   return NULL;
}  

int main() {
   long        threadMin, threadMax, threadStep;
   long        sampleMin, sampleMax, sampleStep;
   long        numTrials;
   pthread_t*  thread_handles; 
   CStopWatch  timer;

   threadMin  = 10; 
   threadMax  = 100;
   threadStep = 10;

   sampleMin  = 1000;
   sampleMax  = 10000;
   sampleStep = 1000;

   numTrials  = 10;
   
   thread_handles = new pthread_t[threadMax]; 

   
   for(int numThreads=threadMin; numThreads<threadMax; numThreads+=threadStep){

      for(long numSamples=sampleMin; numSamples < sampleMax; numSamples+=sampleStep){
         for(int curTrial=0; curTrial<numTrials; curTrial++){
            timer.startTimer();
            for (long thread = 0; thread < numThreads; thread++){
               pthread_create(&thread_handles[thread], NULL, getSamples, (void*) (numSamples/numThreads));  
            }

            for (int thread = 0; thread < numThreads; thread++){
               pthread_join(thread_handles[thread], NULL); 
            }
            timer.stopTimer();

            std::cout << numThreads << " Threads : " << numSamples << " samples " << timer.getElapsedTime() << " seconds\n";;

         }
      }
   }

   delete thread_handles;

   return 0;
} 